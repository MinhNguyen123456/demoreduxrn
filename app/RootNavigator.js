import React, { Component } from 'react';
import {StackNavigator} from 'react-navigation';
import LoginScreen from "./components/LoginView";
import HomeView from "./components/HomeView";

const RootNavigator = StackNavigator({
  Login : {screen: LoginScreen},
  Home: {screen: HomeView},  
});

export default RootNavigator;