
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button, 
  Text,
  View
} from 'react-native';

export default class HomeView extends React.Component{    
    static navigationOptions = {
        title: "Home",
    };
    render(){
        return(
            <View style = {styles.container}>
                <Text>You are login success</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    textInput : {
        width : '100%',
        height: 40,
    },
    containerNoCenter: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });