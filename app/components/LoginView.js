
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button, 
  Text,
  View
} from 'react-native';

import * as actions from '../actions/LoginActions';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_WRONG, NO_LOGIN} from '../actions/LoginActions';
//
function mapStateToProps(state) { return {user : state.userReducer.user}; }
function mapDispatchToProps(dispatch) { return bindActionCreators(actions, dispatch);}
//
export class LoginView extends React.Component{    
    static navigationOptions = {
        title: "Login",
    }
   
    constructor(props){
        super(props)        
        this.state = {
            userNameText: "",
            userPasswordText : ""
        };
    }   
  
    onLogin =  () => {        
        let userName = this.state.userNameText
        this.props.login({userName: this.state.userNameText, password: this.state.userPasswordText});  
    }   
    renderLoginStatus = () =>{
        switch(this.props.user.statusLogin){
            case LOGIN_SUCCESS:
                return <Text>You are logged</Text>
            case LOGIN_WRONG:
                return <Text style={{color: 'red'}}>Wrong user name or password</Text>
            case LOGIN_FAILED:
                return <Text>Login failed</Text>
            default:
                return <Text>Please login</Text>
            }
    }
    componentWillUpdate(){
        console.log("componentWillUpdate")        
    }    

    componentDidUpdate(){        
        if(this.props.user.statusLogin === LOGIN_SUCCESS){            
            console.log("componentDidUpdate") ;
            this.props.navigation.navigate("Home");
        }       
    }

    render(){  
        console.log("render");         
        return(                 
            <View style = {styles.containerNoCenter}>
                <Text >User Name</Text>
                <TextInput style= {styles.textInput} onChangeText={(text) => this.state.userNameText = text}/>
                <Text >Passwrod</Text>
                <TextInput secureTextEntry = {true} style= {styles.textInputPass} onChangeText={(text) => this.state.userPasswordText = text}/>
                <View>
                    {this.renderLoginStatus()}
                </View>
                <Button title = "Login" backgroundColor = "#F5FCFF" onPress = {this.onLogin}/>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    textInput : {
        width : '100%',
        height: 40,
    },
    containerNoCenter: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
  