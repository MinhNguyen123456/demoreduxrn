import {LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_WRONG, NO_LOGIN} from '../actions/LoginActions'

let cloneObj = function(obj) {    
    console.log("mmm : " + obj);
    console.log("lll : " + obj.user.loggeddIn);
    return Object.assign({}, obj);    
}

let newState = function(status = NO_LOGIN ){
    return { user : {statusLogin: status} }
}

export default function(state , action){       
    switch (action.type){
        case LOGIN_SUCCESS:  
            return Object.assign({}, state, newState(LOGIN_SUCCESS))
        case LOGIN_WRONG:
            return Object.assign({}, state, newState(LOGIN_WRONG))
        case LOGIN_FAILED:
            return Object.assign({}, state, newState(LOGIN_FAILED))
        default:
            return  state || newState()
    }
}