import { AppRegistry } from 'react-native';
import React, { Component } from 'react';
import {App} from './App';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import userReducer from './app/reducers/LoginReducer';

let store = createStore(combineReducers({userReducer}));

export default class MyApp extends Component{
    render(){
        return(
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('LoginDemo', () => MyApp);
